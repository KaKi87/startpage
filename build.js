import { build } from 'vite';
import createVuePlugin from '@vitejs/plugin-vue';

(async () => {
    await build({
        root: './src',
        build: {
            outDir: '../dist'
        },
        plugins: [
            createVuePlugin()
        ]
    });
})().catch(console.error);